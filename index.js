const express = require('express')
const port = 5000;

const app = express();

app.use(express.json())

const newUser = {
	firstName : 'levi',
	lastName : 'ackerman',
	age : 28,
	contactNo : '09123456789',
	batchNo : 166,
	email : 'leviAckerman@mail.com',
	password : ' thequickbrownfoxjumpsoverthelazydog'
}
module.exports = {
	newUser : newUser
}


app.listen(port, () => console.log(`Server is running at port ${port}`))