const { factorial , div_check} = require('../src/util.js')

const { expect, assert} = require('chai');

describe('test_fun_factorials',() => {

it('test_fun_factorial_5!_is_120', () =>{
	const product = factorial(5);
	expect(product).to.equal(120);
})

it('test_fun_factorial_1!_is_1', () => {
	const product = factorial(1);
	assert.equal(product,1);
})

it('test if number is even', () =>{
	const product = factorial(5);
	assert.equal(product%2,0)
})
it('test if number is odd', () =>{
	const product = factorial(1);
	assert.equal(product%2,1)
})
})
describe('Activity 1 : result for 0! , 4! , 10!',() => {

it('result for 0!', () =>{
	const div = factorial(0);
	expect(div).to.equal(1);	
})
it('result for 4!', () =>{
	const div = factorial(4);
	expect(div).to.equal(24);
})
it('result for 10!', () =>{
	const div = factorial(10);
	expect(div).to.equal(3628800);
})

})
describe('Activity 2 : Check the number if it is divisible by 5 and 7',() => {

it('divisible by 5', () =>{
	const div = div_check(105);
	expect(div).to.equal(1);
})
it('divisible by 7', () =>{
	const div = div_check(14);
	expect(div).to.equal(1);
})
it('should be divisible by 5 or 7', () =>{
	const div = div_check(0);
	expect(div).to.equal(1);
})
it('should not be divisible by 5 or 7', () =>{
	const div = div_check(0);
	assert.notEqual(div_check,1);
})
})
